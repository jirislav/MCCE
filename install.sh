#!/bin/bash

# Get script directory
DIR=`cd "$(dirname "$0")" && pwd`

# Prepare bash colors
COLOR_OFF="\033[0m"
COLOR_RED="\033[0;31m"
COLOR_GREEN="\033[0;32m"

error() {
	echo -e "${COLOR_RED}$@${COLOR_OFF}\n" >&2
}

success() {
	echo -e "\n${COLOR_GREEN}$@${COLOR_OFF}\n"
}


run() {
    # We need pip3 for functioning ..
	if ! command -v pip3 &>/dev/null; then
		error "pip3 program has to be installed first"
		return 1
	fi

    # Install python3 requirements
	pip3 install -r "$DIR/requirements.txt"

	if test $? -ne 0; then
		error "pip3 failed to install some of the requirements :"
		error "`cat "$DIR/requirements.txt"`"

		if test "$USER" != root; then

			error "pip3 installation typically requires root access, please try to run this script as root:"
			error "sudo -H $0"
			return 3
		fi
		return 2
	fi

	if lsb_release -d 2>/dev/null | grep -q Ubuntu && ! dpkg -l 2>/dev/null | grep -q at-spi2-core; then
		echo 
		echo "You are running on Ubuntu, so you should also install this package:"
		echo " apt install at-spi2-core"
		echo
	fi
}

# Best practices
run $@
exit $?
