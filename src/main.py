#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

"""
Author: Kozlovsky, Jiri <mail@jkozlovsky.cz>
Created: 3.5.18
"""

import sys

from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.QtGui import QIcon

from lib.tabs import TableWidget


class App(QMainWindow):

    def __init__(self, top_level_app):
        super().__init__()
        self.title = "Microorganism's Cells Concentration Estimator (xkozlo04)"
        self.left = 50
        self.top = 50
        self.width = 640
        self.height = 480

        self.setWindowTitle(self.title)
        self.setWindowIcon(QIcon('icon.png'))
        self.setGeometry(self.left, self.top, self.width, self.height)

        self.table_widget = TableWidget(self, self.statusBar, top_level_app)
        self.setCentralWidget(self.table_widget)

        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App(app)
    sys.exit(app.exec_())
