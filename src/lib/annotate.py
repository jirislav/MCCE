"""
Author: Kozlovsky, Jiri <mail@jkozlovsky.cz>
Created: 4.5.18
"""

import os
from glob import glob

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPainter, QKeyEvent, QShowEvent
from PyQt5.QtWidgets import QPushButton, QWidget, QGridLayout, QLabel, QGroupBox, QStatusBar

from lib.helper import open_directory_dialog, extract_attributes_from_filename
from lib.image import AnnotableImage

from lib.container import SharedContainer


class TabAnnotate(QWidget):

    def __init__(self, parent, shared_container, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)

        self.shared_container: SharedContainer = shared_container
        self.statusBar = shared_container.statusBar

        self.layout = QGridLayout(self)
        self.layout.setSpacing(10)

        self.image_name_label = QLabel("Current image name: None")

        self.btn_dir_select = self.directory_selector()

        self.btn_previous = self.get_previous_button()
        self.btn_annotate = self.get_annotate_button()
        self.btn_next = self.get_next_button()

        self.all_images = []
        self.current_image_index = 0
        self.directory = None
        self.directory_name = None

        self.annotating = False

        # Specially computed :) .. but harcoded to make sure user keeps the image size
        self.image_height = 512
        self.image_width = 685

        self.image_container: AnnotableImage = self.get_image_container()
        self.show_image(None)

        # @formatter:off
        self.layout.addWidget(self.btn_dir_select,   0, 0,  1, 5)
        self.layout.addWidget(self.image_name_label, 1, 0,  1, 5)
        self.layout.addWidget(self.image_container,  2, 0, 10, 5)
        self.layout.addWidget(self.btn_previous,    12, 0,  1, 1)
        self.layout.addWidget(self.btn_annotate,    12, 2,  1, 1)
        self.layout.addWidget(self.btn_next,        12, 4,  1, 1)
        # @formatter:on

        self.setLayout(self.layout)

        existing_annotations = list(self.shared_container.annotations.keys())
        if existing_annotations:
            self.pick_annotated_directory(existing_annotations.pop(0))

    def showEvent(self, event: QShowEvent):
        self.statusBar().showMessage(
            "Welcome to the Annotate tab, you can mark important areas on HDR images here, please select a directory."
        )
        self.image_container.setFocus()

    def directory_selector(self):
        title = "Select directory to annotate ..."
        btn = QPushButton(title)
        btn.clicked.connect(lambda: self.pick_annotated_directory(open_directory_dialog(self, title)))
        return btn

    def set_image_name(self, image_name):
        self.image_name_label.setText("Current image name: {}".format('/'.join([self.directory_name, image_name])))

    def get_previous_button(self):
        title = "Previous image (LeftArrow)"
        btn = QPushButton(title)
        btn.clicked.connect(self.goto_previous_image)
        btn.setDisabled(True)
        return btn

    def get_annotate_button(self):
        title = "Annotate this image (Enter)"
        btn = QPushButton(title)
        btn.clicked.connect(self.enter)
        btn.setDisabled(True)
        return btn

    def get_next_button(self):
        title = "Next image (Right Arrow)"
        btn = QPushButton(title)
        btn.clicked.connect(self.goto_next_image)
        btn.setDisabled(True)
        return btn

    def goto_previous_image(self):
        self.current_image_index -= 1
        if self.current_image_index < 0:
            self.current_image_index = len(self.all_images) - 1

        self.show_current_image()

    def goto_next_image(self):
        self.current_image_index += 1
        if self.current_image_index == len(self.all_images):
            self.current_image_index = 0

        self.show_current_image()

    def enter(self):
        # Lock the button until we process the logic
        self.btn_annotate.setDisabled(True)

        switching_elements = (
            self.shared_container.tab_hdr,
            self.shared_container.tab_learn,
            self.shared_container.tab_recognition,
            self.btn_next,
            self.btn_previous,
            self.btn_dir_select,
        )

        for switching_element in switching_elements:
            switching_element.setDisabled(not self.annotating)

        if not self.annotating:

            self.btn_annotate.setText("Cancel")

            # Let's clean up the image container for editing
            self.image_container.setup_scene(read_only=False)

            self.statusBar().showMessage("You are now in annotation mode, please draw circle with left MSB")

        else:

            self.btn_annotate.setText("Annotate this image (Enter)")

            # Let's show the annotation preview
            self.image_container.setup_scene(read_only=True)

            if self.directory in self.shared_container.annotations:
                self.statusBar().showMessage("Done with annotation? Try another directory or go to the Learning tab :)")
            else:
                self.statusBar().showMessage("Pick a picture for annotation (Use Left/Right arrow to see "
                                             "next/previous picture or Enter to choose) ")

        self.annotating = not self.annotating
        self.image_container.setFocus()
        self.btn_annotate.setDisabled(False)

    def circle_added(self):
        count = len(self.image_container.circles)
        if count == 6:
            # We have finished annotating this directory

            current_image = self.all_images[self.current_image_index]
            found_attributes = extract_attributes_from_filename(current_image, also_hdr=True)
            excitation = emission = None
            if found_attributes:
                excitation, emission, _ = found_attributes.groups()

            self.shared_container.store_annotation(
                full_path=self.directory,
                filename=os.path.basename(current_image),
                points=self.image_container.points,
                circles=self.image_container.circles,
                scale=self.image_container.pixmap.scale_me_with,
                excitation=excitation,
                emission=emission
            )

            # Restore functions ..
            self.enter()
        elif count == 1:
            self.statusBar().showMessage("Well done! Now click with your right MSB on any circle to remove it")
        elif count == 2:
            self.statusBar().showMessage("You can also use keyboard (BackSpace for deletion or Esc for Cancel)")
        elif count == 5:
            self.statusBar().showMessage("We are done after you make exactly 6 circles.")

    def show_current_image(self):
        requested_image = self.all_images[self.current_image_index]
        self.show_image(requested_image, True)
        self.set_image_name(os.path.basename(requested_image))
        self.image_container.setFocus()

    def pick_annotated_directory(self, directory):
        if directory == '':
            self.statusBar().showMessage("Cancelled directory selection")
            return

        self.all_images = sorted(glob(os.path.join(directory, 'ex*_em*_hdr.png')))

        if not len(self.all_images):
            self.statusBar().showMessage("You must select directory with png files in format 'exZZZ_emYYY_hdr.png'")
            return

        self.btn_next.setDisabled(False)
        self.btn_annotate.setDisabled(False)
        self.btn_previous.setDisabled(False)

        self.directory = directory
        self.directory_name = os.path.basename(directory)

        self.shared_container.current_directory = self.directory
        self.image_container.changed_directory()

        # Show the first image ..
        self.show_current_image()

        if self.directory in self.shared_container.annotations:
            self.statusBar().showMessage(
                "Annotations were already loaded from previously saved file, but you can override them if you want :)"
            )
        else:
            self.statusBar().showMessage(
                "Pick a picture for annotation (Use Left/Right arrow to see next/previous picture or Enter to choose)"
            )

    def get_image_container(self) -> AnnotableImage:

        # This is needed so that QLabel respects the borders of the image
        # label.setScaledContents(True)

        image = AnnotableImage(self, self.image_width, self.image_height, self.shared_container)
        image.circle_added_event(self.circle_added)
        return image

    def show_image(self, image_path=None, show_stored: bool = False):
        if image_path is None:
            self.image_container.setHidden(True)
        else:
            self.image_container.set_image(self.image_width, self.image_height, image_path, show_stored)
            self.image_container.setHidden(False)
