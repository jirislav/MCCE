"""
Author: Kozlovsky, Jiri <mail@jkozlovsky.cz>
Created: 4.5.18
"""
import io
import os
from typing import Union, Tuple, List

from math import floor, ceil

import PIL
from PIL import Image
import numpy as np
from PIL.PngImagePlugin import PngImageFile

from PyQt5 import QtCore
from PyQt5.Qt import Qt
from PyQt5.QtCore import QThread
from PyQt5.QtGui import QShowEvent
from PyQt5.QtWidgets import QWidget, QGridLayout, QLabel, QPushButton

from lib.container import SharedContainer
from lib.helper import wavelength_to_rgb


class TabLearn(QWidget):
    shared_container_changed = QtCore.pyqtSignal(SharedContainer)

    def __init__(self, parent, shared_container, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)

        self.shared_container: SharedContainer = shared_container
        self.statusBar = shared_container.statusBar

        self.layout = QGridLayout(self)

        self.layout.setSpacing(10)

        self.label_title = QLabel("Available annotations to learn from:")

        self.label_body = QLabel("No data available yet. Please make annotations first!")
        self.label_body.setAlignment(Qt.AlignTop)

        # Should be custom class ..
        self.label_body.setMaximumHeight(512)
        self.label_body.setMaximumWidth(685)
        self.label_body.setWordWrap(True)

        self.btn_learn = self.get_practice_button()

        # @formatter:off
        self.layout.addWidget(QLabel(),         0, 0,  1, 5)  # placeholder
        self.layout.addWidget(self.label_title, 1, 1,  1, 3)
        self.layout.addWidget(self.label_body,  2, 0, 10, 3)
        self.layout.addWidget(self.btn_learn,  12, 2,  1, 1)
        # @formatter:on

        self.setLayout(self.layout)

        self.learning = self.data_showed = False

        # Setup the worker object and the worker_thread.
        self.learner = Learner(self.data_title_updater, self.data_body_updater, self.state_updater)
        self.learner.finished.connect(self.learner_finished)
        self.learner.area_scores_computed_event.connect(self.store_area_scores)
        self.shared_container_changed.connect(self.learner.set_shared_container)

        self.shared_container.top_level_app.aboutToQuit.connect(self.force_learner_quit)

        self.actualize_tab()

    def showEvent(self, event: QShowEvent):
        self.statusBar().showMessage("Welcome to the Learn tab, you can start the learning process here :)")

        self.actualize_tab()
        # self.image_container.setFocus()

    def actualize_tab(self):
        if self.shared_container.annotations and not self.learning and not self.data_showed:
            available_annotations = "\n".join(
                [
                    '/'.join(
                        [
                            os.path.basename(key),
                            item.get('filename', '')
                        ]
                    ) for key, item in self.shared_container.annotations.items()
                ]
            )

            self.label_body.setText(available_annotations)

            self.btn_learn.setDisabled(False)

    def learn(self):

        self.disable_desired()

        if not self.learning:
            self.btn_learn.setDisabled(True)

            self.learning = self.data_showed = True

            self.shared_container_changed.emit(self.shared_container)

            self.learner.start()  # self.shared_container)
            self.label_title.setText('Learning ...')
            self.label_body.setText('')

        else:
            self.learning = False
            self.force_learner_quit()

            self.btn_learn.setDisabled(False)
            self.actualize_tab()

    def disable_desired(self):

        switching_elements = (
            self.shared_container.tab_hdr,
            self.shared_container.tab_annotate,
            self.shared_container.tab_recognition,
        )

        for switching_element in switching_elements:
            switching_element.setDisabled(not self.learning)

    def force_learner_quit(self):
        if self.learner.isRunning():
            self.learner.terminate()
            self.learner.wait()

    def get_practice_button(self):
        title = "Start learning"
        btn = QPushButton(title)
        btn.clicked.connect(self.learn)
        btn.setDisabled(True)
        return btn

    @QtCore.pyqtSlot(str)
    def data_title_updater(self, status: str):
        self.label_title.setText(status)

    @QtCore.pyqtSlot(str)
    def data_body_updater(self, status: str):
        self.label_body.setText(status)

    @QtCore.pyqtSlot(str)
    def state_updater(self, state: str):
        self.statusBar().showMessage(state)

    @QtCore.pyqtSlot(str, dict)
    def store_area_scores(self, directory: str, result: dict):
        self.shared_container.store_knowledge_base(
            directory,
            result
        )

    @QtCore.pyqtSlot()
    def learner_finished(self):

        self.disable_desired()

        self.learning = False
        self.btn_learn.setText('Start learning')
        self.label_title.setText("Learned data:")

        self.label_body.setText('\n'.join([
            self.label_body.text(),
            '\n\n\t\tLearning process has finished! You may proceed to the Recognition tab!'
        ]))
        # self.actualize_tab()


class Learner(QThread):
    change_data_title_event = QtCore.pyqtSignal(str)
    change_data_body_event = QtCore.pyqtSignal(str)
    change_state_event = QtCore.pyqtSignal(str)
    area_scores_computed_event = QtCore.pyqtSignal(str, dict)

    shared_container: SharedContainer = None

    areas: List[
        List[
            Union[
                int,
                float,
                int,
                int
            ]
        ]
    ] = None

    area_scores = None
    area_pixels = None

    min_x: int = 1e6
    min_y: int = 1e6
    max_x: int = 0
    max_y: int = 0

    result = None

    def __init__(self, data_title_updater, data_body_updater, state_updater, parent=None):
        super(self.__class__, self).__init__(parent)

        self.change_data_title_event.connect(data_title_updater)
        self.change_data_body_event.connect(data_body_updater)
        self.change_state_event.connect(state_updater)

    @QtCore.pyqtSlot(SharedContainer)
    def set_shared_container(self, shared_container: SharedContainer):
        self.shared_container = shared_container

    def publish_progress(self, directory: str, filename: str, percent: int, index: int, total: int):
        self.change_state_event.emit('Learning from {}/{} ... {}%'.format(
            directory,
            filename,
            str(percent)
        ))

        self.change_data_title_event.emit('Learning ...    {}%'.format(
            int((100 * index + percent) / total)
        ))

    def reset_bounds(self):
        self.min_x = self.min_y = 1e6
        self.max_x = self.max_y = 0

    def actualize_bounds(self, r, x, y):
        min_x = floor(x - r)
        max_x = ceil(x + r)
        min_y = floor(y - r)
        max_y = ceil(y + r)

        if min_x < self.min_x:
            self.min_x = min_x if min_x >= 0 else 0

        if max_x > self.max_x:
            self.max_x = max_x

        if min_y < self.min_y:
            self.min_y = min_y if min_y >= 0 else 0

        if max_y > self.max_y:
            self.max_y = max_y

    def run(self):
        total_annotations = len(self.shared_container.annotations.keys())

        # self.shared_container = shared_container
        annotation_index = -1
        for directory, annotation in self.shared_container.annotations.items():
            annotation_index += 1
            self.publish_progress(os.path.basename(directory), annotation.get('filename'), 0, annotation_index,
                                  total_annotations)

            self.reset_bounds()

            # Aggregate areas for faster computations
            dict_areas = annotation.get('areas', [])
            scale = annotation.get('scale', 1.0)

            self.areas = list()
            for i, dict_area in enumerate(dict_areas):
                r = dict_area.get('r') / scale
                x = dict_area.get('x') / scale
                y = dict_area.get('y') / scale

                self.areas.append([
                    i,
                    r ** 2,
                    x,
                    y
                ])

                self.actualize_bounds(r, x, y)

            self.area_scores = [0] * len(self.areas)
            self.area_pixels = [0] * len(self.areas)

            filename = annotation.get('filename')
            excitation = annotation.get('excitation')
            emission = annotation.get('emission')
            scale = annotation.get('scale')
            self.learn_from(directory, filename, excitation, emission, scale, annotation_index, total_annotations)

    def learn_from(self, directory, filename, excitation, emission, scale, annotation_index, total_annotations):
        try:

            R, G, B = wavelength_to_rgb(emission)

            image_full_path = os.path.join(directory, filename)
            base_dir = os.path.basename(directory)
            with Image.open(image_full_path) as image:

                # Scale properly
                # image.thumbnail((int(image.width * scale), int(image.height * scale)))

                # Use the RGB :)
                image: PngImageFile = image.convert('RGB')

                # Cut the image
                box = (
                    self.min_x,
                    self.min_y,
                    self.max_x if self.max_x < image.width else image.width - 1,
                    self.max_y if self.max_y < image.height else image.height - 1
                )

                for area in self.areas:
                    area[2] -= box[0]
                    area[3] -= box[1]

                # image.save('/tmp/i_f.png')
                image = image.crop(box)
                # image.save('/tmp/i_c.png')

                y = -1
                for row in np.array(image):
                    y += 1

                    x = -1
                    for r, g, b in row:
                        x += 1

                        # be faster ;) .. ignore black
                        # if r < 20 and g < 20 and b < 20:
                        #     continue

                        underlying_area = self.get_underlying_area(x, y)
                        if underlying_area != -1:
                            # This point lies within one of the areas :)
                            # Here would be interesting to apply the weight for each color by emitted wavelength ..

                            self.area_pixels[underlying_area] += 1

                            # Calculate the score based on the emission frequency :)
                            self.area_scores[underlying_area] += float(R * int(r) + G * int(g) + B * int(b))  # max 255

                            # self.area_scores[underlying_area] += int(g)
                            # self.area_scores[underlying_area] += int(r) + int(g) + int(b)
                            # self.area_scores[underlying_area] += 0.299 * int(r) + 0.587 * int(g) + 0.114 * int(b)

                    if y % 20 == 0:
                        self.publish_progress(base_dir, filename, int(100 * y / image.height),
                                              annotation_index, total_annotations)

                self.result = {
                    'filename': filename,
                    'area_scores': [
                        # Let the score be comparable across all areas by dividing them by the appropriate area
                        score / float(self.area_pixels[i])
                        for i, score in enumerate(self.area_scores)
                    ],
                    'area_organisms_ppx': [
                        total_in_area / float(self.area_pixels[i])
                        for i, total_in_area in enumerate(
                            self.shared_container.concentrations.get(
                                self.shared_container.emission_to_microb_name.get(str(emission), 'algas')
                            )
                        )
                    ],
                    'scale': 1.0,  # The knowledge base is computed from the whole resolution ..
                    'emission': emission,
                    'excitation': excitation
                }

                self.area_scores_computed_event.emit(directory, self.result)
                self.change_data_body_event.emit('Got some results from image "{}/{}":\n\n{}'.format(
                    base_dir,
                    filename,
                    '\n'.join(
                        [
                            'Area {} score: {:6.2f} [-]'.format(
                                index,
                                value
                            ) for index, value in enumerate(self.result['area_scores'])
                        ]
                    )
                ))

                self.publish_progress(base_dir, filename, 100, annotation_index, total_annotations)
        except Exception as e:
            self.change_data_body_event.emit(
                'Failed reading/learning from file: {}/{}\n{}'.format(directory, filename, e))

    def get_underlying_area(self, x, y):
        """
            Returns index of underlying area if found.

            If not found, returns -1.

        :param x:
        :param y:
        :return:
        """
        for i, r_squared, x1, y1 in self.areas:
            if (x1 - x) ** 2 + (y1 - y) ** 2 <= r_squared:
                return i
        return -1

    def __del__(self):
        self.wait()
