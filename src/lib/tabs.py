"""
Author: Kozlovsky, Jiri <mail@jkozlovsky.cz>
Created: 3.5.18
"""

from PyQt5.QtWidgets import QPushButton, QWidget, QTabWidget, QFileDialog, QGridLayout, QLabel, QApplication

from lib.container import SharedContainer
from lib.hdr import TabHDR
from lib.annotate import TabAnnotate
from lib.learn import TabLearn
from lib.recognition import TabRecognition


class TableWidget(QWidget):

    def __init__(self, parent, status_bar, top_level_app: QApplication):
        super(QWidget, self).__init__(parent)

        self.statusBar = status_bar
        self.layout = QGridLayout(self)
        self.layout.setSpacing(10)

        # Create shared container for easy "public" access to accessories
        self.shared_container = SharedContainer()
        self.shared_container.statusBar = self.statusBar
        self.shared_container.top_level_app = top_level_app

        # Initialize tab screen
        self.tabs = QTabWidget(self)

        self.tab_hdr = TabHDR(self.tabs, self.shared_container)
        self.tab_annotate = TabAnnotate(self.tabs, self.shared_container)
        self.tab_learn = TabLearn(self.tabs, self.shared_container)
        self.tab_recognition = TabRecognition(self.tabs, self.shared_container)

        self.tabs.resize(300, 200)

        # Add tabs
        self.tabs.addTab(self.tab_hdr, "HDR")
        self.tabs.addTab(self.tab_annotate, "Annotate")
        self.tabs.addTab(self.tab_learn, "Learn")
        self.tabs.addTab(self.tab_recognition, "Recognition")

        # First tab will be recognition ..
        self.tabs.setCurrentWidget(self.tab_recognition)

        self.shared_container.tab_hdr = self.tab_hdr
        self.shared_container.tab_annotate = self.tab_annotate
        self.shared_container.tab_learn = self.tab_learn
        self.shared_container.tab_recognition = self.tab_recognition

        # Add tabs to widget
        self.layout.addWidget(self.tabs)
        self.setLayout(self.layout)
