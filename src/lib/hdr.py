"""
Author: Kozlovsky, Jiri <mail@jkozlovsky.cz>
Created: 4.5.18
"""

import re
import cv2 as cv
import numpy as np
from PyQt5.QtCore import QEvent
from PyQt5.QtGui import QFocusEvent, QShowEvent

from PyQt5.QtWidgets import QPushButton, QWidget, QGridLayout, QLabel

from lib.container import SharedContainer
from lib.helper import wavelength_to_rgb, open_directory_dialog, extract_attributes_from_filename

import os
from glob import glob

from lib.image import Pixmap


class TabHDR(QWidget):

    def __init__(self, parent, shared_container: SharedContainer, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)

        self.statusBar = shared_container.statusBar
        self.layout = QGridLayout(self)
        self.layout.setSpacing(10)

        self.btn_dir_select = self.directory_selector()

        self.image_height = 512
        self.image_width = 685

        self.current_image = self.image_container()
        self.show_image(None)

        self.layout.addWidget(self.btn_dir_select, 1, 0)
        self.layout.addWidget(self.current_image, 2, 0, 10, 0)

        self.setLayout(self.layout)

        # Do not use this on production ..
        # self.start_hdr('../gallery/2018-02-27_09-35-38-DH')
        # TODO: process HDR in a separate thread

    def showEvent(self, event: QShowEvent):
        self.statusBar().showMessage("Welcome to the HDR tab, you can merge images into one HDR for annotations here.")

    def directory_selector(self):
        title = "Select directory to start the HDR process ..."
        btn = QPushButton(title)
        btn.clicked.connect(lambda: self.start_hdr(open_directory_dialog(self, title)))
        return btn

    def start_hdr(self, directory):
        if directory == '':
            self.statusBar().showMessage("Cancelled directory selection")
            return

        self.statusBar().showMessage("Processing HDR, please wait a while ...")

        all_images = sorted(glob(os.path.join(directory, 'ex*_em*_*.png')))

        if not len(all_images):
            self.statusBar().showMessage("You must select directory with png files in format 'exZZZ_emYYY_XX.png'")
            return

        # Show the first image ..
        self.show_image(all_images[0])

        hdr = HDR(self.statusBar, all_images)
        hdr.run()

        self.statusBar().showMessage("----  Finished processing selected directory with HDR! ----")

    def image_container(self):
        label = QLabel(self)

        label.setMaximumWidth(self.image_width)
        label.setMinimumWidth(self.image_width)

        label.setMaximumHeight(self.image_height)
        label.setMinimumHeight(self.image_height)

        # This is needed so that QLabel respects the borders of the image
        label.setScaledContents(True)

        return label

    def show_image(self, image_path=None):
        if image_path is None:
            self.current_image.setHidden(True)
        else:
            self.current_image.setPixmap(Pixmap(self.image_width, self.image_height, image_path))
            self.current_image.setHidden(False)


class HDR(object):

    exposures_ms = np.array(
        [150.0, 38.73, 10.0] * 3,
        dtype=np.float32
    )

    def __init__(self, statusBar, directory_images: list):

        self.statusBar = statusBar
        self.directory_images = directory_images
        self.interesting_images = self.get_interesting_images()

    def get_interesting_images(self) -> dict:
        interesting_images = {}
        for image in self.directory_images:

            found = extract_attributes_from_filename(image)

            if not found:
                # Skip not interesting images
                continue

            ex, em, no = found.groups()

            if ex not in interesting_images:
                interesting_images[ex] = {}

            if em not in interesting_images[ex]:
                interesting_images[ex][em] = {}

            interesting_images[ex][em][no] = image

        return interesting_images

    def run(self):
        for ex, item in self.interesting_images.items():
            for em, item in item.items():
                self.merge_debvec(ex, em, item)

    def merge_debvec(self, ex, em, item):
        self.statusBar().showMessage("Reading all images ex{ex}_em{em}_XX.png ...".format(
            ex=ex,
            em=em
        ))

        images_content = [cv.imread(image) for image in item.values()]
        merge_debvec = cv.createMergeDebevec()

        # Blok s nepodarenou editaci asimilace odezvy kamery podle pouziteho filtru
        # - nakonec se ukazalo, ze je uplne nejlepsi vysledek pri vychozich hodnotach Debvecova algoritmu

        # em_rgb = wavelength_to_rgb(em)
        # em_rgb = [2 * np.float32(number) / 255 for number in em_rgb]

        # # # Estimate camera response function (CRF)
        # kwargs = {
        #     'samples': 40,
        #     'lambda': np.float32(100),
        #     'random': False
        # }
        # cal_debvec = cv.createCalibrateDebevec(**kwargs)
        # crf_debvec = cal_debvec.process(images_content, times=HDR.exposures_ms.copy())

        # ideal_response = np.array([[em_rgb] for i in range(0, 256)], dtype=np.float32)
        # assimilated_calibration_response = crf_debvec * ideal_response
        # # assimilated_calibration_response = crf_debvec

        # # Merge exposures to HDR image
        # hdr_debvec = merge_debvec.process(images_content, times=HDR.exposures_ms.copy(),
        #                                   response=assimilated_calibration_response)

        self.statusBar().showMessage("Merging images together ex{ex}_em{em}_XX.png ...".format(
            ex=ex,
            em=em
        ))
        hdr_debvec = merge_debvec.process(images_content, times=HDR.exposures_ms.copy())

        self.statusBar().showMessage("Tonemapping images ex{ex}_em{em}_XX.png ...".format(
            ex=ex,
            em=em
        ))
        # Tonemap HDR image
        tonemap_durand = cv.createTonemapDurand(gamma=2.2)
        res_debvec = tonemap_durand.process(hdr_debvec.copy())

        # tonemap_reinhard = cv.createTonemapReinhard(gamma=1.5, intensity=0, light_adapt=1, color_adapt=0)
        # res_debvec = tonemap_reinhard.process(hdr_debvec.copy())

        # Convert datatype to 8-bit and save
        res_debvec_8bit = np.clip(res_debvec * 255, 0, 255).astype('uint8')

        dirname = os.path.dirname(next(iter(item.values())))

        destination_file = os.path.join(
            dirname,
            "ex{ex}_em{em}_hdr.png".format(
                ex=ex,
                em=em
            )
        )
        self.statusBar().showMessage("Writing ex{ex}_em{em}_hdr.png ...".format(
            ex=ex,
            em=em
        ))

        cv.imwrite(destination_file, res_debvec_8bit)
        print("file {} written".format(destination_file))
