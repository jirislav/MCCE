"""
Author: Kozlovsky, Jiri <mail@jkozlovsky.cz>
Created: 4.5.18
"""

import os
from glob import glob

import scipy.interpolate
import numpy as np

from PIL import Image
from PIL.PngImagePlugin import PngImageFile

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm

from PyQt5.QtGui import QPainter, QKeyEvent, QShowEvent
from PyQt5.QtWidgets import QPushButton, QWidget, QGridLayout, QLabel, QGroupBox, QStatusBar

from lib.helper import open_directory_dialog, extract_attributes_from_filename, wavelength_to_rgb
from lib.image import AnnotableImage

from lib.container import SharedContainer


class TabRecognition(QWidget):

    def __init__(self, parent, shared_container, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)

        self.shared_container: SharedContainer = shared_container
        self.statusBar = shared_container.statusBar

        self.layout = QGridLayout(self)
        self.layout.setSpacing(10)

        self.image_name_label = QLabel("Current image name: None")

        self.btn_dir_select = self.directory_selector()

        self.btn_previous = self.get_previous_button()
        self.btn_recognize = self.get_annotate_button()
        self.btn_next = self.get_next_button()

        self.all_images = []
        self.current_image_index = 0
        self.directory = None
        self.directory_name = None
        self.population_density_map: dict = None

        self.recognizing = False

        # Specially computed :) .. but harcoded to make sure user keeps the image size
        self.image_height = 512
        self.image_width = 685

        self.image_container = self.get_image_container()
        self.show_image(None)

        # @formatter:off
        self.layout.addWidget(self.btn_dir_select,   0, 0,  1, 5)
        self.layout.addWidget(self.image_name_label, 1, 0,  1, 5)
        self.layout.addWidget(self.image_container,  2, 0, 10, 5)
        self.layout.addWidget(self.btn_previous,    12, 0,  1, 1)
        self.layout.addWidget(self.btn_recognize,   12, 2,  1, 1)
        self.layout.addWidget(self.btn_next,        12, 4,  1, 1)
        # @formatter:on

        self.setLayout(self.layout)

    def showEvent(self, event: QShowEvent):

        existing_knowledge_base = list(self.shared_container.knowledge_base.keys())

        switching_elements = (
            self.btn_next,
            self.btn_recognize,
            self.btn_previous,
            self.btn_dir_select,
        )

        for switching_element in switching_elements:
            switching_element.setDisabled(not existing_knowledge_base)

        if existing_knowledge_base:
            if self.hdr_directory_picker(existing_knowledge_base.pop(0)):
                self.statusBar().showMessage("Welcome to the Recognition tab, you can load specific HDR images to "
                                             "recognize the microorganism's layout! ")
        else:
            nope = "You must first obtain knowledge! See the Learn tab."
            self.image_name_label.setText(nope)
            self.statusBar().showMessage(nope)

        self.image_container.setFocus()

    def directory_selector(self):
        title = "Select directory with HDR images to recognize the microorganism's density!"
        btn = QPushButton(title)
        btn.clicked.connect(lambda: self.hdr_directory_picker(open_directory_dialog(self, title)))
        return btn

    def set_image_name(self, image_name):
        self.image_name_label.setText("Current image name: {}".format('/'.join([self.directory_name, image_name])))

    def get_previous_button(self):
        title = "Previous image (LeftArrow)"
        btn = QPushButton(title)
        btn.clicked.connect(self.goto_previous_image)
        btn.setDisabled(True)
        return btn

    def get_annotate_button(self):
        title = "Detect microbes on this image (Enter)"
        btn = QPushButton(title)
        btn.clicked.connect(self.enter)
        btn.setDisabled(True)
        return btn

    def get_next_button(self):
        title = "Next image (Right Arrow)"
        btn = QPushButton(title)
        btn.clicked.connect(self.goto_next_image)
        btn.setDisabled(True)
        return btn

    def goto_previous_image(self):
        self.current_image_index -= 1
        if self.current_image_index < 0:
            self.current_image_index = len(self.all_images) - 1

        self.show_current_image()

    def goto_next_image(self):
        self.current_image_index += 1
        if self.current_image_index == len(self.all_images):
            self.current_image_index = 0

        self.show_current_image()

    def enter(self):
        # Lock the button until we process the logic
        self.btn_recognize.setDisabled(True)

        switching_elements = (
            self.shared_container.tab_hdr,
            self.shared_container.tab_annotate,
            self.shared_container.tab_learn,
            self.btn_next,
            self.btn_previous,
            self.btn_dir_select,
        )

        for switching_element in switching_elements:
            switching_element.setDisabled(not self.recognizing)

        if not self.recognizing:

            self.btn_recognize.setText("Cancel")

            # Running recognition ...
            try:
                self.recognize()
            except KeyboardInterrupt:
                self.shared_container.top_level_app.quit()

        else:

            self.btn_recognize.setText("Detect microbes on this image (Enter)")

            # Cancelling recognition ...
            plt.close('all')

        self.recognizing = not self.recognizing
        self.image_container.setFocus()
        self.btn_recognize.setDisabled(False)

    def get_knowledge_interpolation_function(self, filename):
        for item_knowledge in self.shared_container.knowledge_base.values():
            if filename == item_knowledge.get('filename'):
                area_scores = item_knowledge.get('area_scores')
                area_organisms_ppx = item_knowledge.get('area_organisms_ppx')

                # Define that anything under score 50 is considered without any organism
                area_scores.append(50)
                area_organisms_ppx.append(0)

                area_scores.append(0)
                area_organisms_ppx.append(0)

                # Set the maximum value to be only 5% more than the measured ..
                area_scores.insert(0, 255)
                area_organisms_ppx.insert(0, max(area_organisms_ppx) * 1.05)

                # Let's use cubic interpolation for smoother results ...
                fn = scipy.interpolate.interp1d(
                    area_scores,
                    area_organisms_ppx,
                    kind='slinear',
                    # kind='quadratic',
                    # kind='cubic',
                    fill_value='extrapolate',
                    assume_sorted=False,
                    bounds_error=False
                )

                plt.interactive(False)
                fig = plt.figure(1)
                ax = fig.add_subplot(111)
                score_range = np.arange(0, 255)
                ax.plot(score_range, fn(score_range))
                ax.set(
                    xlabel='Pixel emission frequency intensity',
                    ylabel='Count of organisms per px',
                    title='Function of organisms ppx based on pixel emission intensity'
                )

                plt.interactive(True)
                plt.show()

                return fn

        return lambda x: 0

    def recognize(self):

        self.statusBar().showMessage('Calculating the population density, it may take a few minutes, please wait ...')

        image_full_path = self.all_images[self.current_image_index]
        filename = os.path.basename(image_full_path)

        score_to_orgnaism_count_ppx = self.get_knowledge_interpolation_function(filename)
        excitation, emission, _ = extract_attributes_from_filename(image_full_path, also_hdr=True).groups()
        R, G, B = wavelength_to_rgb(emission)

        base_dir = os.path.basename(self.directory)
        with Image.open(image_full_path) as image:

            # Change this to 1 for full image quality (but longer calculations)
            scale = 0.7
            image.thumbnail((int(image.width * scale), int(image.height * scale)))

            # Use the RGB :)
            image: PngImageFile = image.convert('RGB')

            population_density_x = np.arange(0, image.width)
            population_density_y = np.arange(0, image.height)
            population_density_x, population_density_y = np.meshgrid(population_density_x, population_density_y)

            population_density_z = []

            y = -1
            for row in np.array(image):
                y += 1

                population_density_z_row = []

                x = -1
                for r, g, b in row:
                    x += 1

                    # Calculate the score based on the emission frequency :)
                    pixel_score = float(R * int(r) + G * int(g) + B * int(b))  # max 255
                    organisms_at_this_pixel = score_to_orgnaism_count_ppx(pixel_score)

                    population_density_z_row.append(organisms_at_this_pixel)

                population_density_z.append(np.array(population_density_z_row))

                if y % 5 == 0:
                    self.statusBar().showMessage('Processing the image ... {} %'.format(
                        int(100 * y / image.height)
                    ))

            self.population_density_map = {
                'x': population_density_x,
                'y': population_density_y,
                'z': np.array(population_density_z)
            }

            self.statusBar().showMessage('Processing the image ... 100 %  --> Showing you the results')

            self.plot_population_density_map(emission)

    def plot_population_density_map(self, emission):
        # Close first figure first ..

        plt.interactive(False)
        fig = plt.figure(2)
        ax_3d = fig.add_subplot(111, projection='3d')
        # ax_3d = Axes3D(fig)
        # surface = ax_3d.plot_trisurf(
        #     self.population_density_map['x'],
        #     self.population_density_map['y'],
        #     self.population_density_map['z'],
        #     linewidth=0.1
        # )
        # fig.colorbar(surface, shrink=0.5, aspect=5)

        ax_3d.plot_surface(
            self.population_density_map['x'],
            self.population_density_map['y'],
            self.population_density_map['z']
        )

        ax_3d.set(
            xlabel='Image width [px]',
            ylabel='Image height [px]',
            zlabel='Count of {} per px'.format(
                self.shared_container.emission_to_microb_name.get(str(emission), 'algas')
            ),
            title='Function of organisms ppx based on pixel emission intensity'
        )

        # Set initial view to somewhat similar user sees on the preview ..
        ax_3d.view_init(74, 52)

        # We see images with point [0, 0] at the left top corner ..
        plt.gca().invert_xaxis()

        # plt.savefig('/tmp/plot.pdf')

        # When False, the user can move around ..
        plt.interactive(False)
        plt.show()

    def show_current_image(self):
        requested_image = self.all_images[self.current_image_index]
        self.show_image(requested_image, True)
        self.set_image_name(os.path.basename(requested_image))
        self.image_container.setFocus()

    def has_knowledge(self, file_full_path: str) -> bool:
        for learned_item in self.shared_container.knowledge_base.values():
            if os.path.basename(file_full_path) == learned_item.get('filename'):
                # Only permit files of which we have some kind of knowledge how to process it ..
                return True
        return False

    def hdr_directory_picker(self, directory) -> bool:
        if directory == '':
            self.statusBar().showMessage("Cancelled directory selection")
            return False

        self.all_images = sorted(filter(self.has_knowledge, glob(os.path.join(directory, 'ex*_em*_hdr.png'))))

        if not len(self.all_images):
            self.statusBar().showMessage(
                "ERR: You must select directory with png files in format 'exZZZ_emYYY_hdr.png' of which we have some "
                "knowledge ! "
            )
            return False

        self.btn_next.setDisabled(False)
        self.btn_recognize.setDisabled(False)
        self.btn_previous.setDisabled(False)

        self.directory = directory
        self.directory_name = os.path.basename(directory)

        self.shared_container.current_directory = self.directory
        self.image_container.changed_directory()

        # Show the first image ..
        self.show_current_image()

        return True

    def get_image_container(self) -> AnnotableImage:

        # This is needed so that QLabel respects the borders of the image
        # label.setScaledContents(True)

        image = AnnotableImage(self, self.image_width, self.image_height, self.shared_container)
        return image

    def show_image(self, image_path=None, show_stored: bool = False):
        self.image_container.set_image(self.image_width, self.image_height, image_path, show_stored)
