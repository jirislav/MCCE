"""
Author: Kozlovsky, Jiri <mail@jkozlovsky.cz>
Created: 4.5.18
"""
from typing import List

from PyQt5.QtGui import QPixmap, QPainter, QImage, QMouseEvent, QFont, QColor, QPen, QKeyEvent
from PyQt5.QtCore import QSize, Qt, QPoint, QRectF
from PyQt5.QtWidgets import QLabel, QWidget, QGraphicsView, QGraphicsScene, QFrame, QGraphicsPixmapItem, \
    QGraphicsObject, QGraphicsTextItem, QGraphicsItem, QGraphicsRectItem, QGraphicsEllipseItem, QStyleOptionGraphicsItem


class Pixmap(QPixmap):
    original_width = None
    original_height = None

    scale_me_with = None

    def __init__(self, image_width, image_height, image_path=None, *__args):

        if not image_path:
            # Just dummy pixmap
            super().__init__(image_width, image_height)

            self.original_width = self.width()
            self.original_height = self.height()

        else:
            super().__init__(image_path)

            self.original_width = self.width()
            self.original_height = self.height()

            self.scaled(
                QSize(image_width, image_height),
                Qt.KeepAspectRatio,
                Qt.SmoothTransformation
            )

        self.scale_me_with = float(image_width) / float(self.original_width)


class AnnotableImage(QGraphicsView):
    _drawn_points: int = 0
    _drawn_circles: int = 0
    _drawn_numbers: int = 0

    _point_items: List[QGraphicsItem] = list()
    _circle_items: List[QGraphicsItem] = list()
    _number_items: List[QGraphicsItem] = list()

    points: List[QPoint] = list()
    circles: List[float] = list()
    numbers: List[QPoint] = list()

    mouse_listening: bool = False

    # To be overriden by parent
    circle_added_callback = lambda self: None

    def __init__(self, parent, width, height, shared_container, *__args):
        super().__init__(parent, *__args)

        from lib.container import SharedContainer
        self.shared_container: SharedContainer = shared_container

        self.pixmap = Pixmap(image_width=width, image_height=height, image_path=None)
        self._width = width
        self._height = height

        self.setScene(QGraphicsScene(self))
        self.setup_scene(False)

        self.setRenderHint(QPainter.Antialiasing, True)
        self.setFrameStyle(QFrame.NoFrame)

        self.setMaximumWidth(self._width)
        self.setMinimumWidth(self._width)

        self.setMaximumHeight(self._height)
        self.setMinimumHeight(self._height)

    def changed_directory(self):

        if self.shared_container.annotations is not None:
            if self.shared_container.current_directory in self.shared_container.annotations:
                for i, area in enumerate(
                        self.shared_container.annotations[self.shared_container.current_directory]['areas']
                ):
                    middle = QPoint(area['x'], area['y'])
                    self.drawPoint(middle)
                    self.drawCircle(middle, area['r'])

    def set_image(self, width, height, image_path, show_stored: bool = False):
        self.pixmap = Pixmap(image_width=width, image_height=height, image_path=image_path)
        self.setup_scene(show_stored)

    def setup_scene(self, read_only: bool):
        scene = self.scene()

        self._drawn_points = 0
        self._drawn_circles = 0
        self._drawn_numbers = 0
        self._point_items = list()
        self._circle_items = list()
        self._number_items = list()
        self.points = list()
        self.circles = list()

        # So that PyCharm can show details ..
        if isinstance(scene, QGraphicsScene):
            scene.clear()
            bg = scene.addPixmap(self.pixmap)

            if isinstance(bg, QGraphicsPixmapItem):
                bg.setScale(self.pixmap.scale_me_with)
                bg.setZValue(0)
                bg.setPos(0, 0)

        if read_only:
            self.changed_directory()
            # Ignore all mouse events, because we want only show the status ..
            self.mouse_listening = False
        else:
            # Enable interacting with the image ..
            self.mouse_listening = True

        scene.setSceneRect(0, 0, self._width, self._height)

    def drawPoint(self, clicked_at):

        empty = EmptyGraphicsObject()
        empty.setZValue(1)
        self._point_items.append(empty)

        # Draw really small rectangle :)
        rect = QGraphicsRectItem(clicked_at.x() - 3, clicked_at.y() - 3, 6, 6, empty)
        rect.setPen(QPen(QColor("white"), 1))

        self.points.append(clicked_at)
        self._redraw_points(incremented=True)

        self.drawNumber(clicked_at, self._drawn_numbers)

    def drawNumber(self, clicked_at, number):

        empty = EmptyGraphicsObject()
        empty.setZValue(1)
        self._number_items.append(empty)

        # Draw really small rectangle :)
        item = QGraphicsTextItem(str(number), empty)
        item.setX(clicked_at.x())
        item.setY(clicked_at.y())
        item.setDefaultTextColor(QColor("white"))

        self.numbers.append(clicked_at)
        self._redraw_numbers(incremented=True)

    def _redraw_numbers(self, incremented: bool, index: int = -1):

        if incremented:
            self.scene().addItem(self._number_items[index])
            self._drawn_numbers += 1
        else:
            self.scene().removeItem(self._number_items[index])
            self._drawn_numbers -= 1

    def _redraw_points(self, incremented: bool, index: int = -1):

        if incremented:
            self.scene().addItem(self._point_items[index])
            self._drawn_points += 1
        else:
            self.scene().removeItem(self._point_items[index])
            self._drawn_points -= 1

    def _redraw_circles(self, incremented: bool, index: int = -1):

        if incremented:
            self.scene().addItem(self._circle_items[index])
            self._drawn_circles += 1
        else:
            self.scene().removeItem(self._circle_items[index])
            self._drawn_circles -= 1

    def drawCircle(self, middle_of_circle, radius):

        empty = EmptyGraphicsObject()
        empty.setZValue(1)
        self._circle_items.append(empty)

        radius_int = int(radius)
        diameter_int = 2 * radius_int

        rect = QRectF(
            middle_of_circle.x() - radius_int,
            middle_of_circle.y() - radius_int,
            diameter_int,
            diameter_int
        )

        circle = QGraphicsArcItem(rect, empty)
        circle.setPen(QPen(QColor("white"), 1))

        self.circles.append(radius)
        self._redraw_circles(True)

    def remove_item(self, index: int = -1):

        if len(self.points) == 0 and index == -1:
            self.setup_scene(read_only=True)
            self.parent().enter()
            return True

        # If we are in the middle of adding a circle, don't do anything .. also if there is nothing to remove ;)
        if len(self.points) == len(self.circles) and self._drawn_points == self._drawn_circles:
            # remove last circle
            self._redraw_circles(False, index)
            self._circle_items.pop(index)
            self.circles.pop(index)

            # remove last point
            self._redraw_points(False, index)
            self._point_items.pop(index)
            self.points.pop(index)

            self._redraw_numbers(False, index)
            self._number_items.pop(index)
            self.numbers.pop(index)

            return True
        return False

    def mousePressEvent(self, event: QMouseEvent):
        if self.mouse_listening:
            clicked_at = event.pos()

            if event.button() == Qt.LeftButton:

                # Adding area by clicking on it
                self.drawPoint(clicked_at)
                event.accept()

            elif event.button() == Qt.RightButton:

                # Removing area by right clicking on it
                for i, item in enumerate(self._circle_items):
                    if item.childrenBoundingRect().contains(clicked_at):
                        self.remove_item(i)
                        event.accept()
                        break
        else:
            event.ignore()

    def mouseReleaseEvent(self, event: QMouseEvent):
        if self.mouse_listening:

            if event.button() == Qt.LeftButton:
                if len(self.points) > len(self.circles):
                    middle_of_circle = self.points[-1]
                    released_at = event.pos()

                    radius = (
                                     float(released_at.x() - middle_of_circle.x()) ** 2.0 +
                                     float(released_at.y() - middle_of_circle.y()) ** 2.0
                             ) ** (1.0 / 2.0)

                    self.drawCircle(middle_of_circle, radius)

                    self.circle_added_callback()

                    event.accept()
        else:
            event.ignore()

    def keyPressEvent(self, event: QKeyEvent):
        from lib.annotate import TabAnnotate
        from lib.recognition import TabRecognition

        key = event.key()
        parent = self.parent()
        scene = self.scene()
        if (
                isinstance(parent, TabAnnotate) or isinstance(parent, TabRecognition)

        ) and isinstance(scene, QGraphicsScene):
            if not self.mouse_listening:
                if key == Qt.Key_Left:
                    parent.goto_previous_image()
                    event.accept()
                elif key == Qt.Key_Right:
                    parent.goto_next_image()
                    event.accept()
                elif key in (Qt.Key_Enter, Qt.Key_Return):
                    parent.enter()
                    event.accept()
            elif self.mouse_listening:
                if key == Qt.Key_Escape:
                    self.setup_scene(read_only=True)
                    parent.enter()
                    event.accept()
                elif key == Qt.Key_Backspace:
                    if self.remove_item(-1):
                        event.accept()

    def circle_added_event(self, fn):
        if callable(fn):
            self.circle_added_callback = fn


class EmptyGraphicsObject(QGraphicsObject):
    def __init__(self, parent: QGraphicsItem = None):
        super().__init__(parent=parent)

    def boundingRect(self) -> QRectF:
        return QRectF(0, 0, 0, 0)

    def paint(*args, **kwargs) -> None:
        pass


class QGraphicsArcItem(QGraphicsEllipseItem):
    def __init__(self, qrect: QRectF, parent=None):
        """
            QGraphicsArcItem ( qreal x, qreal y, qreal width, qreal height, QGraphicsItem * parent = 0 ) :
                QGraphicsEllipseItem(x, y, width, height, parent) {

            Taken from:
                https://stackoverflow.com/questions/14279162/qt-qgraphicsscene-drawing-arc#14279360

        :param x:
        :param y:
        :param width:
        :param height:
        :param parent:
        """

        super().__init__(qrect, parent)

    def paint(self, painter: QPainter, option: QStyleOptionGraphicsItem, widget: QWidget = None):
        painter.setPen(self.pen())
        painter.setBrush(self.brush())
        painter.drawArc(self.rect(), self.startAngle(), self.spanAngle())

        # if (option->state & QStyle::State_Selected)
        #    qt_graphicsItem_highlightSelected(this, painter, option);
