"""
Author: Kozlovsky, Jiri <mail@jkozlovsky.cz>
Created: 4.5.18
"""

import os
import json
from pprint import pprint
from typing import List, Dict, Union

from PyQt5.QtCore import QPoint
from PyQt5.QtWidgets import QStatusBar, QApplication


class SharedContainer(object):
    VERSION: str = "1.2.0"
    LATEST_COMPATIBLE_VERSION: str = "1.1.0"

    statusBar: QStatusBar = None

    top_level_app: QApplication = None

    annotations: Dict[
        str,
        Dict[
            str,
            Union[
                List[
                    Dict[
                        str,
                        Union[
                            float,
                            int
                        ]
                    ]
                ],
                str,
                float
            ]
        ]
    ] = dict()

    knowledge_base: Dict[
        str,
        Dict[
            str,
            Union[
                str,
                List[
                    float
                ]
            ]
        ]
    ] = dict()

    emission_to_microb_name = {
        '465': 'algas',
        '420': 'cyanobacteria',
        '520': 'bacteria'
    }

    # Taken from Popis.txt ...
    concentrations = {
        'algas': [
            240e3,
            120e3,
            60e3,
            30e3,
            15e3,
            7.5e3
        ],
        'cyanobacteria': [
            11e6,
            5.5e6,
            2.75e6,
            1.375e6,
            687.5e3,
            343.75e3
        ],
        'bacteria': [
            180e6,
            90e6,
            45e6,
            22.5e6,
            11.25e6,
            5.625e6
        ]
    }

    current_directory: str = None

    location = '../gallery/shared_container.json'

    def __init__(self):

        from lib.hdr import TabHDR
        self.tab_hdr: TabHDR = None

        from lib.annotate import TabAnnotate
        self.tab_annotate: TabAnnotate = None

        from lib.learn import TabLearn
        self.tab_learn: TabLearn = None

        from lib.recognition import TabRecognition
        self.tab_recognition: TabRecognition = None

        try:
            with open(self.location, 'r') as fp:
                data = json.load(fp)

                # Only accept the same and higher version (there is a need for backward compatibility because of this)
                if data.get('version', '') >= self.LATEST_COMPATIBLE_VERSION:
                    self.annotations = data.get('annotations', self.annotations)
                    self.knowledge_base = data.get('knowledge_base', self.knowledge_base)

        except FileNotFoundError:
            pass

    def __repr__(self):
        return repr(self.get_data())

    def store_knowledge_base(self, directory: str, learned_result: dict):

        self.knowledge_base[directory] = learned_result

        self.save()

        self.statusBar().showMessage("Knowledge base for '{dir_name}' was saved to '{loc}'".format(
            dir_name=os.path.basename(directory),
            loc=self.location
        ))

    def store_annotation(
            self,
            full_path: str,
            filename: str,
            points: List[QPoint],
            circles: List[float],
            scale: float,
            excitation: int,
            emission: int
    ):

        areas_list = list()

        self.annotations[full_path] = {
            'areas': areas_list,
            'filename': filename,
            'scale': scale,
            'excitation': excitation,
            'emission': emission
        }

        for point, radius in zip(points, circles):
            areas_list.append({
                'x': point.x(),
                'y': point.y(),
                'r': radius
            })

        self.save()

        self.statusBar().showMessage("Annotations for '{dir_name}' were saved to '{loc}'".format(
            dir_name=os.path.basename(full_path),
            loc=self.location
        ))

    def save(self):

        with open(self.location, 'w') as fp:
            json.dump(self.get_data(), fp, sort_keys=True, indent=4)

        pprint(self)

    def get_data(self):
        return {
            'annotations': self.annotations,
            'knowledge_base': self.knowledge_base,
            'version': self.VERSION
        }
