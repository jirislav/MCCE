"""
Author: Kozlovsky, Jiri <mail@jkozlovsky.cz>
Created: 4.5.18
"""
import re
from typing import Tuple

from PyQt5.QtWidgets import QFileDialog


def open_directory_dialog(qt_object, what_to_ask):
    return str(QFileDialog.getExistingDirectory(qt_object, what_to_ask))


def extract_attributes_from_filename(file_name, also_hdr=False):
    """
        returns re.match() object with groups in the following order:
        excitation, emission, file number

    :param file_name:
    :param also_hdr:
    :return:
    """
    interesting_images_regex = re.compile(r'^.*/ex(\d+)_em(\d+)_(\d+{}).png$'.format(
        r'|hdr' if also_hdr else ''
    ))
    return re.match(interesting_images_regex, file_name)


def wavelength_to_rgb(wavelength, gamma=0.8) -> Tuple[float, float, float]:
    """
        Inspired by https://aty.sdsu.edu/explain/optics/rendering.html
    """
    wavelength = float(wavelength)
    if 380 <= wavelength <= 440:
        attenuation = 0.3 + 0.7 * (wavelength - 380) / (440 - 380)
        R = ((-(wavelength - 440) / (440 - 380)) * attenuation) ** gamma
        G = 0.0
        B = (1.0 * attenuation) ** gamma
    elif 440 <= wavelength <= 490:
        R = 0.0
        G = ((wavelength - 440) / (490 - 440)) ** gamma
        B = 1.0
    elif 490 <= wavelength <= 510:
        R = 0.0
        G = 1.0
        B = (-(wavelength - 510) / (510 - 490)) ** gamma
    elif 510 <= wavelength <= 580:
        R = ((wavelength - 510) / (580 - 510)) ** gamma
        G = 1.0
        B = 0.0
    elif 580 <= wavelength <= 645:
        R = 1.0
        G = (-(wavelength - 645) / (645 - 580)) ** gamma
        B = 0.0
    elif 645 <= wavelength <= 750:
        attenuation = 0.3 + 0.7 * (750 - wavelength) / (750 - 645)
        R = (1.0 * attenuation) ** gamma
        G = 0.0
        B = 0.0
    else:
        R = 0.0
        G = 0.0
        B = 0.0
    # R *= 255
    # G *= 255
    # B *= 255
    return R, G, B
