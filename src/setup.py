from setuptools import setup

setup(
    name='MCCE',
    version='0.1.0',
    packages=['lib'],
    package_dir={'': 'src'},
    url='https://www.gitlab.com/jirislav/MCCE',
    license='Apache License 2.0',
    author='jirislav',
    author_email='mail@jkozlovsky.cz',
    description='Microorganism\'s Cells Concentration Estimator'
)
