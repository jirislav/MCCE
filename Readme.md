# MCCE

 - Microorganism's Cells Concentration Estimator

## Installation

```bash
git clone https://gitlab.com/jirislav/MCCE.git
cd MCCE
pip3 install -r requirements.txt
```

## Running the MCCE

```bash
src/main.py
```
